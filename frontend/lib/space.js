const nacl = require('tweetnacl')
const blake2b = require('blake2b')
const hex = require('./hex.js')
const tomlParse = require('@iarna/toml/parse-string')
const RCWS = require('./rcws.js')

const TE = new TextEncoder
const TD = new TextDecoder
const SALT_BYTES = blake2b.SALTBYTES // 16
const APP_ID_BYTES = 4
const HASH_BYTES = 32
const SECRET_BYTES = nacl.secretbox.keyLength // 32
const MAGIC = [ 0x77, 0xeb, 0xdc ]

module.exports = class Space {
  constructor({ secret, base, meta={} }) {
    this.hashes = new Map
    this.secret = secret
    this.base = base ?? ''
    this.meta = meta ?? {}
    this.meta.apps = this.meta.apps ?? new Map
    if (!(this.meta.apps instanceof Map)) {
      this.meta.apps = new Map(Object.entries(this.meta.apps ?? {}))
    }
    this._lastSerial = -1
    this._pingId = new Uint8Array(8)
    this._activeListeners = new Set
    this._updateControllers = new Map
    this._maxSerial = new Map
  }

  static async load({ secret, base }) {
    let space = new Space({ secret, base })
    await space.loadMeta()
    return space
  }
  static async create({ base, meta={} }) {
    let secret = crypto.getRandomValues(new Uint8Array(SALT_BYTES + SECRET_BYTES))
    console.log('CREATE', meta)
    let space = new Space({ secret, base, meta })
    await space.saveMeta()
    return space
  }
  getSecret() {
    return this.secret.slice(SALT_BYTES)
  }
  getSalt() {
    return this.secret.slice(0, SALT_BYTES)
  }
  hash(file) {
    let h = this.hashes.get(file)
    if (h !== undefined) return h
    h = blake2b(HASH_BYTES, this.getSecret(), this.getSalt())
      .update(TE.encode(file))
      .digest('hex')
    this.hashes.set(file, h)
    return h
  }

  async get(file) {
    let id = this.hash(file)
    let idBytes = hex.toBytes(id)
    console.log('GET',file,id)
    let response = await this.rawGet(id)
    if (!(200 <= response.status && response.status < 300)) {
      let err = new Error(response.statusText ?? 'status code ' + response.status)
      err.status = response.status
      err.response = response
      throw err
    }
    let data = new Uint8Array(await response.arrayBuffer())
    if (!arrayEq(data, 0, MAGIC.length, MAGIC, 0, 3)) {
      throw new Error('magic number not found in blob result')
    }
    let idBody = this.decrypt(data.slice(MAGIC.length))
    if (idBody === null) {
      throw new Error(`failed to decrypt file ${file}`)
    }
    let msgId = hex.fromBytes(idBody.slice(0, idBytes.length))
    if (msgId !== id) {
      throw new Error('payload decrypted from server does not have matching id')
    }
    return idBody.slice(idBytes.length)
  }
  put(file, body) {
    let id = this.hash(file)
    let idBytes = hex.toBytes(id)
    console.log('PUT',file,id, body)

    if (typeof body === 'string') body = TE.encode(body)
    return this.rawPut(id, u8concat([ MAGIC, this.encrypt(u8concat([ idBytes, body ])) ]))
  }
  delete(file) {
    console.log('DELETE',file,id)
    let id = this.hash(file)
    return this.rawDelete(id)
  }

  async loadMeta() {
    this.meta = {}
    try {
      this.meta = JSON.parse(TD.decode(await this.get('space.json')))
      console.log('loaded=', this.meta)
    } catch (err) {
      if (err.status !== 404) console.log(err)
    }
    this.meta.apps = new Map(Object.entries(this.meta.apps ?? {}))
  }
  async saveMeta() {
    await this.put('space.json', this.serializeMeta())
  }
  serializeMeta() {
    return JSON.stringify({
      name: this.meta?.name ?? '',
      apps: Object.fromEntries(this.meta?.apps ?? new Map),
    })
  }

  async uploadApp({ filename, icon, manifest, archive }) {
    if (!/\.xdc$/.test(filename)) {
      throw new Error('app must have a .xdc extension')
    }
    let appId = this.createAppId()
    let dir = 'app/' + appId
    manifest = manifest ? tomlParse(TD.decode(manifest)) : {}
    this.meta.apps.set(appId, {
      icon: icon?.filename,
      manifest: manifest,
      filename,
    })
    let actions = [
      this.put(dir + '/' + filename, archive),
      this.saveMeta(),
    ]
    if (icon) actions.push(this.put(dir + '/' + icon.filename, icon.data))
    if (manifest) actions.push(this.put(dir + '/manifest.toml', manifest))
    await Promise.all(actions)
    return appId
  }

  createAppId() {
    while (true) {
      let appId = hex.fromBytes(crypto.getRandomValues(new Uint8Array(APP_ID_BYTES)))
      if (!this.meta.apps.has(appId)) return appId
    }
  }

  async rawPut(id, payload) {
    let response = await fetch(`${this.base}/_webxdc/blob/${id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/octet-stream',
      },
      redirect: 'follow',
      body: payload,
    })
    return await response.json()
  }
  async rawGet(id) {
    return await fetch(`${this.base}/_webxdc/blob/${id}`, {
      method: 'GET',
      redirect: 'follow',
    })
  }
  async rawDelete(id) {
    let response = await fetch(`${this.base}/_webxdc/blob/${id}`, {
      method: 'DELETE',
      redirect: 'follow',
    })
    return await response.json()
  }

  async setUpdateListener(appId, serial, fn) {
    let self = this
    console.log('SET UPDATE LISTENER', appId, serial)

    if (this._activeListeners.has(appId)) {
      this.clearUpdateListener(appId)
    }
    if (!fn) return
    this._activeListeners.add(appId)

    let last = Date.now()
    let updates = await this.getUpdates(appId, `${serial}..-1`)
    updates.forEach(callListener)

    ;(async function f() {
      if (!self._activeListeners.has(appId)) return
      last = Date.now()
      let updates = []
      let wait = 0
      try {
        updates = await self.getUpdates(appId, `${serial}..`)
      } catch (err) {
        if (err.name === 'AbortError') return
        console.error(err)
        wait = 5000
      }
      if (!self._activeListeners.has(appId)) return
      if (updates.length === 0) wait = Math.max(wait, last+5000 - Date.now())
      updates.forEach(callListener)
      setTimeout(f, wait)
    })()

    function callListener(update) {
      self._maxSerial.set(appId, Math.max(self._maxSerial.get(appId) ?? 0, update.serial))
      console.log('UPDATE', appId, update.serial, update)
      serial = Math.max(serial, update.serial + 1)
      fn({
        serial: update.serial,
        data: Object.assign({ max_serial: self._maxSerial.get(appId) }, update.data),
      })
    }
  }

  clearUpdateListener(appId) {
    console.log('CLEAR UPDATE LISTENER', appId)
    this._updateControllers.get(appId)?.abort()
    this._updateControllers.delete(appId)
    this._activeListeners.delete(appId)
    this._maxSerial.delete(appId)
  }

  async sendUpdate(appId, data) {
    let updatesId = this.hash(`updates/${appId}`)
    let payload = this.encrypt(JSON.stringify(data))
    let response = await fetch(`${this.base}/_webxdc/updates/${updatesId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/octet-stream',
      },
      redirect: 'follow',
      body: payload,
    })
    try {
      let msg = await response.json()
      if (msg.ok) {
        console.log(`sent update to ${updatesId}, serial=${msg.serial}`)
      } else {
        console.error(msg)
      }
    } catch (err) {
      console.error(err)
    }
  }

  async getUpdates(appId, range) {
    let updatesId = this.hash(`updates/${appId}`)
    let url = `${this.base}/_webxdc/updates/${updatesId}/${range}`
    console.log('GET UPDATES', url)
    let controller = new AbortController
    this._updateControllers.get(appId)?.abort()
    this._updateControllers.set(appId, controller)
    let timeout = setTimeout(() => {
      controller.abort()
    }, 110*1000)
    let response
    try {
      response = await fetch(url, {
        method: 'GET',
        redirect: 'follow',
        signal: controller.signal,
      })
    } catch (err) {
      if (err.name !== 'AbortError') console.error(err)
      return []
    } finally {
      clearTimeout(timeout)
    }
    let body = new Uint8Array(await response.arrayBuffer())
    let offset = 0
    let updates = []
    while (offset < body.length) {
      let len = u32FromBE(body, offset+0)
      let serial = u32FromBE(body, offset+4)
      let str = TD.decode(this.decrypt(body.slice(offset+8, offset+8+len)))
      let data = JSON.parse(str)
      updates.push({ serial, data })
      offset += 8 + len
    }
    return updates
  }

  encrypt(body) {
    if (typeof body === 'string') body = TE.encode(body)
    if (body instanceof ArrayBuffer) body = new Uint8Array(body)
    if (!(body instanceof Uint8Array)) throw new Error('encrypt() called on unexpected input type')
    let nonce = crypto.getRandomValues(new Uint8Array(nacl.secretbox.nonceLength))
    let enc = nacl.secretbox(body, nonce, this.getSecret())
    return u8concat([ nonce, enc ])
  }

  decrypt(payload) {
    if (payload instanceof ArrayBuffer) payload = new Uint8Array(payload)
    if (!(payload instanceof Uint8Array)) throw new Error('decrypt() called on unexpected input type')
    let nonce = payload.slice(0, nacl.secretbox.nonceLength)
    let enc = payload.slice(nacl.secretbox.nonceLength)
    return nacl.secretbox.open(enc, nonce, this.getSecret())
  }
}

function u8concat(xs) {
  let size = 0
  for (let i = 0; i < xs.length; i++) {
    let x = xs[i]
    if (Array.isArray(x) || x instanceof Uint8Array) size += x.length
    else if (typeof x === 'string') size += TE.encode(x).length
    else size++
  }
  let out = new Uint8Array(size)
  let offset = 0
  for (let i = 0; i < xs.length; i++) {
    let x = xs[i]
    if (Array.isArray(x) || x instanceof Uint8Array) {
      out.set(x, offset)
      offset += x.length
    } else if (typeof x === 'string') {
      let s = TE.encode(x)
      out.set(s, offset)
      offset += s.length
    } else {
      out[offset++] = x
    }
  }
  return out
}

function arrayEq(a, aStart, aEnd, b, bStart, bEnd) {
  if (aEnd - aStart !== bEnd - bStart) return false
  for (let k = 0; k < aEnd-aStart; k++) {
    if (a[k+aStart] !== b[k+bStart]) return false
  }
  return true
}

function u32FromBE(buf, offset) {
  return (buf[offset+0] << 24) | (buf[offset+1] << 16) | (buf[offset+2] << 8) | (buf[offset+3] << 0)
}

function u32ToBE(x) {
  return [ (x >> 24) % 256, (x >> 16) % 256, (x >> 8) % 256, (x >> 0) % 256 ]
}
