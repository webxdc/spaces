// reconnecting websocket

module.exports = class RCWS {
  constructor(opts) {
    this.address = opts.address
    this._ping = opts.ping ?? 30 * 1000
    this._timeout = {
      open: opts.openTimeout ?? 15 * 1000,
      ping: opts.pingTimeout ?? 15 * 1000,
    }
    this._count = {
      connection: 0,
    }
    this._last = {
      open: -1,
      close: -1,
      ping: -1,
      pong: -1,
    }
    this._queue = []

    let onOpen = opts.onOpen ?? noop
    this._isOpen = false
    this._onOpen = ev => {
      this._count.connection++
      this._isOpen = true
      this._onInfo(`connected to ${this.address}`)
      this._last.open = Date.now()
      clearTimeout(this._socketTimeout)

      for (let msg of this._queue) {
        console.log('QUEUED', msg)
        this.socket.send(msg)
      }
      this._queue = []

      this._pingIv = setInterval(() => {
        if (!this._isOpen) return
        this._onInfo(`sending ping to ${this.address}`)
        this._pingTimeout = setTimeout(() => {
          this._onInfo('timed out receiving ping from server')
          this._destroy()
          this._reconnect()
        }, this._timeout.ping)
        this._last.ping = Date.now()
        this.socket.send(this._onPing())
      }, this._ping)

      onOpen(ev)
      if (this._count.connection > 1) this._onReconnect()
    }

    let onClose = opts.onClose ?? noop
    this._onClose = ev => {
      this._onInfo(`closed connection to ${this.address}`)
      this._last.close = Date.now()
      this._isOpen = false
      this._destroy()
      onClose(ev)
      this._reconnect()
    }

    let onError = opts.onError ?? noop
    this._onError = ev => {
      this._onInfo(`websocket error from ${this.address}: ${ev.message}`)
      onError(ev)
    }
    this._onMessage = opts.onMessage ?? noop
    this._onInfo = opts.onInfo ?? noop
    this._onPing = opts.onPing ?? noop
    this._onReconnect = opts.onReconnect ?? noop
    this.connect()
  }

  send(msg) {
    if (this._isOpen) this.socket.send(msg)
    else this._queue.push(msg)
  }

  _connectBackoff() {
    return Math.max(0, 15*1000 - Date.now() + this._last.open)
  }
  _reconnect() {
    if (this._backoffTimeout) return
    this._backoffTimeout = setTimeout(() => {
      this._backoffTimeout = null
      this.connect()
    }, this._connectBackoff())
  }

  connect() {
    if (this._backoffTimeout) {
      clearTimeout(this._backoffTimeout)
      this._backoffTimeout = null
    }
    this._onInfo(`connecting to ${this.address}`)
    this.socket = new WebSocket(this.address)

    this._socketTimeout = setTimeout(() => {
      this._onInfo(`connection timed out to ${this.address}`)
      this._destroy()
      this._reconnect()
    }, 1000 * this._timeout.open)

    this.socket.addEventListener('open', this._onOpen)
    this.socket.addEventListener('close', this._onClose)
    this.socket.addEventListener('error', this._onError)
    this.socket.addEventListener('message', this._onMessage)
  }

  pong() {
    this._last.pong = Date.now()
    this._onInfo(`received pong from ${this.address}`)
    clearTimeout(this._pingTimeout)
  }

  _destroy() {
    this._isOpen = false
    this.socket.removeEventListener('open', this._onOpen)
    this.socket.removeEventListener('close', this._onClose)
    this.socket.removeEventListener('error', this._onError)
    this.socket.removeEventListener('message', this._onMessage)
    clearTimeout(this._openTimeout)
    clearTimeout(this._pingTimeout)
    clearInterval(this._pingIv)
    clearTimeout(this._backoffTimeout)
    this._backoffTimeout = null
    this.socket.close()
  }

  disconnect() {
    this._destroy()
  }
}

function noop() {}
