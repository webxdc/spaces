module.exports = async (filter={}) => {
  let input = document.createElement('input')
  input.setAttribute('type', 'file')
  input.style.display = 'none'

  let accept = (filter.mimeTypes ?? []).concat(filter.extensions ?? []).join(',')
  input.setAttribute('accept', accept)
  if (filter.multiple) input.setAttribute('multiple', 'multiple')

  let cb = null
  input.addEventListener('change', function f(ev) {
    cb.resolve(ev.target.files)
    cb = null
    input.removeEventListener('change', f)
  })

  var ev = document.createEvent('MouseEvents')
  ev.initEvent('click', true, false)
  input.dispatchEvent(ev)

  return new Promise((resolve,reject) => {
    cb = { resolve, reject }
  })
}
