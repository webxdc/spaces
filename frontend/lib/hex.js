exports.fromBytes = function to(bytes) {
  let out = ''
  for (let i = 0; i < bytes.length; i++) {
    out += bytes[i].toString(16).padStart(2, '0')
  }
  return out
}

exports.toBytes = function from(str) {
  let out = new Uint8Array(str.length/2)
  for (let i = 0; i < out.length; i++) {
    out[i] = parseInt(str.charAt(i*2+0),16)*16 +  parseInt(str.charAt(i*2+1),16)
  }
  return out
}
