const app = require('choo')({ hash: true })

app.use(require('./store/error.js'))
app.use(require('./store/space.js'))
app.use(require('./store/service.js'))
app.use(require('./store/create-space.js'))
app.use(require('./store/check-features.js'))
app.use(require('./store/chat.js'))
app.use(require('./store/load.js'))
app.use(require('./store/frame.js'))

app.route('/:secret/:app_id', require('./view/app.js'))
app.route('/:secret', require('./view/space.js'))
app.route('/', require('./view/create.js'))

app.mount(document.body)
