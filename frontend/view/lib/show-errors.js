const html = require('choo/html')

module.exports = function showErrors(state,emit) {
  if (state.errors.length === 0) return ''
  return html`<div class="errors">
    ${state.errors.map(err => html`<div class="error">Error: ${err.message}</div>`)}
    <button onclick=${clearErrors}>dismiss</button>
  </div>`
  function clearErrors(ev) { emit('error:clear') }
}
