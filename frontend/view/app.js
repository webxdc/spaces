const html = require('choo/html')
const hex = require('../lib/hex.js')

module.exports = (state,emit) => {
  let appIframe = state.iframe ?? html`<div class="info">loading...</div>`
  return html`<body class="app-view">
    <div class="app-title">
      <div class="name">
        ${state.app.manifest?.name ?? state.params.app_id}
      </div>
      <div class="buttons">
        <button onclick=${closeApp}>close</button>
      </div>
    </div>
    <div class="app-pane">${appIframe}</div>
  </body>`
  function closeApp() {
    emit('service:uninstall')
    emit('pushState', '/#' + hex.fromBytes(state.space.secret))
  }
}
