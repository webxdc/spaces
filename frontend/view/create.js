const html = require('choo/html')
const showErrors = require('./lib/show-errors.js')

module.exports = (state,emit) => {
  return html`<body>
    <div class="create-space">
      <h1>webxdc spaces</h1>
      ${showErrors(state,emit)}
      <form onsubmit=${createSpace}>
        <div>
          <input type="text" name="name" value=${state.createSpace.name ?? ''}>
          <button>create a new space</button>
        </div>
        <div>
          <button class="de-emphasize" onclick=${regen}>regenerate name</button>
        </div>
      </form>
    </div>
  </body>`
  function createSpace(ev) {
    ev.preventDefault()
    emit('space:create', { name: ev.target.elements.name.value })
  }
  function regen(ev) {
    ev.preventDefault()
    emit('create-space:generate-name')
  }
}
