const html = require('choo/html')
const showErrors = require('./lib/show-errors.js')
const hex = require('../lib/hex.js')
const fileDialog = require('../lib/file-dialog.js')

module.exports = (state,emit) => {
  let appList = html`<div class="info">loading...</div>`
  if (state.space && state.loaded.apps) {
    appList = Array.from((state.space.meta.apps ?? new Map).entries()).map(([appId,app]) => {
      return html`<div class="app">
        ${app.iconSrc
          ? html`<button class="app-icon" onclick=${() => launchApp(appId)}>
            <img class="icon" src=${app.iconSrc}>
          </button>`
          : ''
        }
        <div class="name">${app.manifest?.name ?? appId}</div>
      </div>`
    })
  }
  let name = state.space?.meta?.name ?? '...'
  let buttons = []
  if (state.uiSpace.saving) {
    buttons.push(state.uiSpace.spinner)
  } else {
    buttons.push(html`<button
      class="edit" title="edit" alt="edit"
      onclick=${editName}
    >\u270e</button>`)
  }
  if (state.uiSpace.editing) {
    buttons.push(html`<button
      class="edit" title="cancel" alt="cancel"
      onclick=${cancelEditName}
    >\u2718</button>`)
  }

  return html`<body class="apps-view">
    <h1>
      <form class="edit-name" onsubmit=${submitEdit}>
        ${state.uiSpace.editing
          ? html`<input type="text" name="name" value=${name}>`
          : name
        }
      </form>
      ${buttons}
      <button class="upload" onclick=${uploadApp}>+</button>
    </h1>
    ${showErrors(state,emit)}
    <div class="app-list">${appList}</div>
    <div class="space-chat">
      <button onclick=${toggleInfoExpand}
        >info ${state.chat.expand ? '\u2b9d' : '\u2b9f'}</button>
      <div class="details ${state.chat.expand ? '' : 'hidden'}">
        ${state.chat.updates.map(update => {
          return html`<div>${JSON.stringify(update)}</div>`
        })}
      </div>
    </div>
  </body>`

  function toggleInfoExpand(ev) {
    ev.preventDefault()
    emit('chat:toggle-info')
  }
  async function uploadApp(ev) {
    let files = await fileDialog({
      extensions: '.xdc',
    })
    if (files[0]) emit('app:upload', files[0])
  }
  function launchApp(appId) {
    emit('pushState', '/#' + hex.fromBytes(state.space.secret) + '/' + appId)
    emit('app:launch', appId)
  }
  function editName(ev) {
    ev.preventDefault()
    emit('space:toggle-edit-name')
  }
  function cancelEditName(ev) {
    ev.preventDefault()
    emit('space:edit-name-cancel')
  }
  function editChange(ev) {
    state.space.meta.name = ev.target.value
  }
  async function submitEdit(ev) {
    console.log('SUBMIT EDIT')
    ev.preventDefault()
    state.space.meta.name = ev.target.elements.name.value
    emit('space:saving-edit-start')
    emit('render')
    console.log('SAVE META', state.space.meta)
    try {
      await state.space.saveMeta()
      emit('space:send-meta-update', '_chat', {
        context: '_space',
        data: { text: 'renamed the space' },
      })
      await state.space.saveMeta()
    } finally {
      emit('space:saving-edit-end')
    }
    emit('space:edit-name-off')
    emit('render')
  }
}
