module.exports = (state,emitter) => {
  state.errors = []
  state.nextErrorId = 0

  window.addEventListener('error', err => {
    emitter.emit('error:push', err)
  })

  emitter.on('error:push', err => {
    console.log(err)
    let id = state.nextErrorId++
    state.errors.push({ id, message: String(err?.message ?? err) })
    emitter.emit('render')
  })
  emitter.on('error:clear', () => {
    state.errors = []
    emitter.emit('render')
  })
}
