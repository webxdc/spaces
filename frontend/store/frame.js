const fileDialog = require('../lib/file-dialog.js')
const getSpace = require('./lib/get-space.js')

module.exports = (state,emitter) => {
  state.iframe = null
  emitter.on('service:installed', () => {
    state.iframe = document.createElement('iframe')
    state.iframe.addEventListener('load', async () => {
      if (!state.iframe.contentWindow) return
      let space = await getSpace(state, emitter)
      let hash = space.hash(`socket/${state.app.id}`)
      let wsproto = location.protocol === 'https:' ? 'wss' : 'ws'
      let socket = `${wsproto}://${location.host}/_webxdc/socket/${hash}`
      state.iframe.contentWindow.postMessage({
        type: 'info',
        serial: 0, // TODO
        socket,
      })
    })
    state.iframe.setAttribute('src', '/app.html')
    emitter.emit('render')
  })
  emitter.on('service:uninstall', () => {
    state.iframe = null
    state.space.clearUpdateListener(state.app.id)
    emitter.emit('render')
  })
  window.addEventListener('message', async ev => {
    if (!state.iframe || ev.source !== state.iframe.contentWindow) return
    if (ev.data.type === 'setUpdateListener') {
      state.space.setUpdateListener(
        state.app.id,
        ev.data.serial,
        update => {
          state.iframe?.contentWindow.postMessage({
            type: 'update',
            serial: update.serial,
            data: update.data,
          })
        },
      )
    } else if (ev.data.type === 'sendUpdate') {
      state.space.sendUpdate(state.app.id, ev.data.data)
      let info = ev.data.data?.info
      if (info !== undefined) {
        state.space.sendUpdate('_chat', {
          context: state.app.id,
          data: { text: info },
        })
      }
    } else if (ev.data.type === 'sendToChat') {
      // TODO: check this and save attachment separately as a blob
      state.space.sendUpdate('_chat', {
        context: state.app.id,
        data: ev.data.data
      })
    } else if (ev.data.type === 'importFiles') {
      let files = await fileDialog()
      state.iframe.contentWindow.postMessage({
        type: 'import-files-reply',
        files
      })
    }
  })
}
