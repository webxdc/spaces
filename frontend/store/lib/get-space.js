module.exports = async function getSpace(state, emitter) {
  return new Promise((resolve, reject) => {
    if (state.space) resolve(state.space)
    else emitter.once('space:ready', () => {
      resolve(state.space)
    })
  })
}
