const hex = require('../lib/hex.js')

module.exports = (state,emitter) => {
  state.loading = { apps: false }
  state.loaded = { apps: false }
  state.page = {
    current: null,
  }

  emitter.once('space:ready', (where) => {
    state.loading.apps = false
    state.loaded.apps = true
  })

  emitter.on('navigate', () => {
    if (state.page.current) {
      emitter.emit('page:close', state.page.current)
      emitter.emit('page:close:' + state.page.current)
    }
    state.page.current = getPage(state.params)
    emitter.emit('page:open', state.page.current)
    emitter.emit('page:open:' + state.page.current)
  })

  emitter.on('DOMContentLoaded', () => {
    state.page.current = getPage(state.params)
    emitter.emit('page:open', state.page.current)
    emitter.emit('page:open:' + state.page.current)
  })

  emitter.on('page:open', page => {
    if (state.params.secret && !state.space) {
      state.loading.apps = true
      emitter.emit('space:load', hex.toBytes(state.params.secret))
    }
    if (page === 'app') {
      emitter.once('space:ready', () => {
        emitter.emit('app:launch', state.params.app_id)
      })
    }
    emitter.emit('service:load')
  })
}

function getPage(params) {
  if (params.secret && params.app_id) {
    return 'app'
  } else if (params.secret) {
    return 'space'
  } else {
    return 'create'
  }
}
