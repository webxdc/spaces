module.exports = (state,emitter) => {
  state.createSpace = {
    name: null,
    nouns: null,
    adjectives: null,
    loading: false,
  }

  emitter.on('page:open', page => {
    if (page === 'create') {
      emitter.emit('create-space:generate-name')
    }
  })

  emitter.on('create-space:generate-name', async () => {
    if (state.createSpace.loading) return
    state.createSpace.name = null
    if (state.createSpace.nouns === null) {
      state.createSpace.loading = true
      try {
        Object.assign(state.createSpace, await getWords())
      } finally {
        state.createSpace.loading = false
      }
    }
    if (state.createSpace.name !== null) return
    let { adjectives, nouns } = state.createSpace
    let adj = adjectives[Math.floor(adjectives.length*Math.random())]
    let noun = nouns[Math.floor(nouns.length*Math.random())]
    let name = `${adj} ${noun} space`
    state.createSpace.name = name
    emitter.emit('render')
  })
}

async function getWords() {
  let adjs = fetch('/_webxdc/words/s_adj')
  let nouns = fetch('/_webxdc/words/s_noun')
  ;[adjs,nouns] = await Promise.all([ adjs, nouns ])
  ;[adjs,nouns] = await Promise.all([ adjs.text(), nouns.text() ])
  return {
    adjectives: adjs.split(/\r?\n/),
    nouns: nouns.split(/\r?\n/),
  }
}
