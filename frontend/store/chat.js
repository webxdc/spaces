const getSpace = require('./lib/get-space.js')

module.exports = (state,emitter) => {
  state.chat = {
    updates: [],
    maxSerial: 0,
    firstLoad: true,
    timer: null,
    expand: false,
  }

  emitter.on('chat:push', update => {
    state.chat.updates.push(update)
    if (update.data.context === '_space' && !state.chat.firstLoad
    && !state.updates.local.has(update.data.id)) {
      emitter.emit('space:load-meta')
    }
  })

  emitter.on('page:open:space', async () => {
    let space = await getSpace(state, emitter)
    space.setUpdateListener(
      '_chat',
      state.chat.maxSerial,
      update => {
        state.chat.maxSerial = Math.max(update.serial, state.chat.maxSerial)
        emitter.emit('chat:push', update)
        if (!state.chat.timer && state.chat.firstLoad) {
          // block of first messages arrives in one chunk on the same tick
          state.chat.timer = setTimeout(() => {
            state.chat.firstLoad = false
          }, 0)
        }
        emitter.emit('render', 250)
      },
    )
  })

  emitter.on('page:close:space', async () => {
    let space = await getSpace(state, emitter)
    space.clearUpdateListener('_chat')
  })

  emitter.on('chat:toggle-info', () => {
    state.chat.expand = !state.chat.expand
    emitter.emit('render')
  })
}
