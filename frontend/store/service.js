module.exports = (state, emitter) => {
  state.service = {
    active: null,
    installed: null,
  }
  let readyResolves = []
  function serviceReady() {
    if (state.service.active) return Promise.resolve(state.service.active)
    return new Promise((resolve, reject) => {
      readyResolves.push(resolve)
    })
  }

  emitter.on('service:ready', sw => {
    console.log('service worker is ready')
    readyResolves.forEach(resolve => {
      resolve(state.service.active)
    })
    readyResolves = []
  })

  emitter.on('service:install', async app => {
    let msg = { type: 'install', id: app.id, files: app.files }
    let sw = await serviceReady()
    sendMsg(sw, msg, data => {
      if (data.ok) {
        state.service.installed = app.id
        console.log('INSTALLED', app.id)
        emitter.emit('service:installed')
      } else {
        emitter.emit('error:push', data.error)
      }
    })
  })

  emitter.on('service:uninstall', async () => {
    let sw = await serviceReady()
    sw.postMessage({
      type: 'uninstall',
      id: state.service.installed,
    })
    state.service.installed = null
  })

  emitter.on('service:load', async () => {
    console.log('loading service worker')
    if (!navigator.serviceWorker) {
      return emitter.emit('error:push', new Error('service workers not supported'))
    }
    let reg
    try {
      navigator.serviceWorker.register('/_webxdc_service.js', { scope: '/' })
      reg = await navigator.serviceWorker.ready
    } catch (err) {
      return emitter.emit('error:push', err)
    }
    state.service.active = reg.active
    emitter.emit('service:ready', reg.active)
  })
}

function sendMsg(sw, msg, cb) {
  let messageChannel = new MessageChannel()
  messageChannel.port1.onmessage = ev => cb(ev.data)
  sw.postMessage(msg, [ messageChannel.port2 ])
}
