module.exports = (state,emitter) => {
  if (typeof DecompressionStream !== 'function') {
    emitter.emit('error:push', "this browser doesn't support DecompressionStream,"
      + ' which is required to unzip files')
  }
}
