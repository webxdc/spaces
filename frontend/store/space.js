const Space = require('../lib/space.js')
const hex = require('../lib/hex.js')
const Unzip = require('../lib/unzip.js')

module.exports = (state,emitter) => {
  state.app = {
    loaded: false,
    loading: false,
    id: null,
    manifest: null,
  }
  state.uiSpace = {
    prevName: null,
    editing: false,
    saving: false,
    spinner: null,
  }
  state.updates = {
    local: new Set
  }

  emitter.on('space:toggle-edit-name', () => {
    if (state.uiSpace.editing) {
      emitter.emit('space:edit-name-off')
    } else {
      emitter.emit('space:edit-name-on')
    }
    emitter.emit('render')
  })
  emitter.on('space:edit-name-off', () => {
    state.uiSpace.editing = false
    state.uiSpace.prevName = null
    emitter.emit('render')
  })
  emitter.on('space:edit-name-on', () => {
    state.uiSpace.editing = true
    state.uiSpace.prevName = state.space.meta.name
    emitter.emit('render')
  })
  emitter.on('space:edit-name-cancel', () => {
    state.uiSpace.editing = false
    state.space.meta.name = state.uiSpace.prevName
    state.uiSpace.prevName = null
    emitter.emit('render')
  })

  emitter.on('space:saving-edit-start', () => {
    state.uiSpace.spinner = document.createElement('span')
    state.uiSpace.saving = true
    let parts = [ '▛','▜','▟','▙' ]
    let i = 0
    state.uiSpace.spinnerInterval = setInterval(() => {
      state.uiSpace.spinner.textContent = parts[i++%parts.length]
    }, 250)
    emitter.emit('render')
  })
  emitter.on('space:saving-edit-end', () => {
    state.uiSpace.saving = false
    clearInterval(state.uiSpace.spinnerInterval)
    emitter.emit('render')
  })

  emitter.on('space:create', async (meta) => {
    state.space = await Space.create({ meta })
    emitter.emit('pushState', '/#' + hex.fromBytes(state.space.secret))
    await sendUpdate('_chat', {
      context: '_space',
      data: { text: 'created a new space' },
    })
    emitter.emit('space:ready')
    emitter.emit('render')
  })
  emitter.on('space:load', async (secret) => {
    state.space = await Space.load({ secret })
    emitter.emit('space:ready')
    emitter.emit('space:meta-loaded', state.space.meta)
    emitter.emit('render')
    emitter.emit('app:load-icons')
  })
  emitter.on('space:load-meta', async () => {
    console.log('LOAD META')
    await state.space?.loadMeta()
    emitter.emit('space:meta-loaded', state.space.meta)
    emitter.emit('app:load-icons')
    emitter.emit('render')
  })

  emitter.on('service:installed', () => {
    state.app.loaded = true
    state.app.loading = false
  })

  emitter.on('app:launch', async appId => {
    localStorage.clear()

    if (state.service.installed) {
      emitter.emit('service:uninstall')
    }

    let app = state.space.meta.apps.get(appId)
    state.app.id = appId
    state.app.loaded = false
    state.app.loading = true
    state.app.manifest = app.manifest
    let buffer = await state.space.get('app/' + appId + '/' + app.filename)
    let unzip = new Unzip({ DecompressionStream, buffer })
    let files = new Map
    try {
      while (true) {
        let entry = unzip.next()
        if (!entry) break
        if (entry.signature !== Unzip.LOCAL_FILE_HEADER) continue
        files.set(entry.filename, await unzip.readFile(entry))
      }
    } catch (err) {
      console.log(err)
      return emitter.emit('error:push', err)
    }
    emitter.emit('service:install', { id: appId, files })
    emitter.emit('render')
  })

  emitter.on('app:load-icons', async () => {
    for (let [appId,app] of state.space.meta.apps) {
      if (!app.icon) continue
      let iconFile = 'app/' + appId + '/' + app.icon
      state.space.get(iconFile).then(imgData => {
        app.iconSrc = URL.createObjectURL(new Blob([imgData]))
        emitter.emit('render')
      })
    }
  })

  emitter.on('app:upload', async (file) => {
    console.log('app:upload', file)
    const reader = new FileReader()
    reader.readAsArrayBuffer(file)
    reader.addEventListener('loadend', async (ev) => {
      let buffer = new Uint8Array(ev.target.result)
      let unzip = new Unzip({ DecompressionStream, buffer })
      let upload = {
        filename: file.name,
        manifest: null,
        icon: null,
        archive: buffer,
      }
      let foundIndex = false
      try {
        while (true) {
          let entry = unzip.next()
          if (!entry) break
          if (entry.signature !== Unzip.LOCAL_FILE_HEADER) continue
          if (entry.filename === 'index.html') {
            foundIndex = true
          } else if (entry.filename === 'manifest.toml') {
            upload.manifest = await unzip.readFile(entry)
          } else if (entry.filename === 'icon.png' || entry.filename === 'icon.jpg') {
            upload.icon = {
              filename: entry.filename,
              data: await unzip.readFile(entry),
            }
          }
        }
      } catch (err) {
        console.log(err)
        return emitter.emit('error:push', err)
      }
      if (!foundIndex) {
        return emitter.emit('error:push', new Error('index.html not found in archive'))
      }
      console.log('upload app', upload)
      let appId
      try {
        appId = await state.space.uploadApp(upload)
      } catch (err) {
        return emitter.emit('error:push', err)
      }
      await sendUpdate('_chat', {
        context: '_space',
        data: {
          text: `added application ${upload.manifest?.name ?? ''} [${appId}]`
        },
      })
      emitter.emit('render')
      emitter.emit('app:load-icons')
    })
    reader.addEventListener('error', err => {
      emitter.emit('error:push', err)
    })
  })

  emitter.on('space:send-meta-update', async (feed, update) => {
    await sendUpdate(feed, update)
  })

  async function sendUpdate(feed, update) {
    let id = hex.fromBytes(crypto.getRandomValues(new Uint8Array(4)))
    state.updates.local.add(id)
    await state.space.sendUpdate(feed, Object.assign({ id }, update))
  }
}
