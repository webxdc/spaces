# webxdc-app-spaces

## prerequisites

* rust / cargo
* libsqlite3-dev
* nodejs / npm
* make

## running

To build and run the server on the default `127.0.0.1:8080`:

```
make
```

or to use a different interface or port:

```
make args='--listen 127.0.0.1:5000'
```

## developing

To build the frontend, watching for changes:

```
make install-frontend # first-time only or when deps change
make dev-frontend
```

and separately, to build and run the backend:

```
make run-backend
```
