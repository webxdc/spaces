use async_std::{io, task, stream::StreamExt};
use tide::{prelude::json,Body};
use tide_websockets::{Message,WebSocket,WebSocketConnection};
use clap::Parser;
use futures::future;

mod blob_store;
use blob_store::BlobStore;
mod updates;
use updates::Updates;

mod config;
use config::{Config, Args};

mod channel;
use channel::Channels;

#[derive(Clone)]
struct State {
    pub blobs: BlobStore,
    pub updates: Updates,
    pub channels: Channels,
}

impl State {
    async fn open(config: Config) -> Result<Self,Box<std::io::Error>> {
        Ok(Self {
            blobs: BlobStore::new(&config),
            updates: Updates::open(&config).await.unwrap(),
            channels: Channels::default(),
        })
    }
}

#[async_std::main]
async fn main() -> Result<(), Box<std::io::Error>> {
    let config: Config = Args::parse().into();

    tide::log::start();
    let listen = config.listen.clone();
    let mut app = tide::with_state(State::open(config).await?);
    app.at("/_webxdc/blob/:id").get(get_blob);
    app.at("/_webxdc/blob/:id").put(put_blob);
    app.at("/_webxdc/blob/:id").delete(delete_blob);
    app.at("/_webxdc/updates/:id/:range").get(get_updates);
    app.at("/_webxdc/updates/:id").put(push_update);
    app.at("/_webxdc/socket/:id")
        .with(WebSocket::new(handle_socket))
        .get(|_| async move { Ok("not a websocket request") });
    app.at("/").serve_file("public/index.html")?;
    app.at("/").serve_dir("public")?;
    app.listen(&listen).await?;
    Ok(())
}

async fn get_blob(req: tide::Request<State>) -> tide::Result {
    let id = req.param("id")?;
    let blobs = req.state().blobs.clone();
    match blobs.get(id).await {
        Err(e) => Ok(wrap_io_error(e)),
        Ok(reader) => {
            let body = Body::from_reader(reader, None);
            Ok(tide::Response::builder(tide::StatusCode::Ok).body(body).build())
        },
    }
}

async fn put_blob(mut req: tide::Request<State>) -> tide::Result {
    let id = req.param("id")?.to_string();
    let blobs = req.state().blobs.clone();
    match blobs.put(&id, &mut req).await {
        Err(e) => Ok(wrap_io_error(e)),
        Ok(()) => Ok(json!({ "ok": true }).into()),
    }
}

async fn delete_blob(req: tide::Request<State>) -> tide::Result {
    let id = req.param("id")?;
    let blobs = req.state().blobs.clone();
    match blobs.delete(id).await {
        Err(e) => Ok(wrap_io_error(e)),
        Ok(()) => Ok(json!({ "ok": true }).into()),
    }
}

fn wrap_io_error(e: io::Error) -> tide::Response {
    let mut res = tide::Response::new(match e.kind() {
        io::ErrorKind::InvalidInput => 400,
        io::ErrorKind::NotFound => 404,
        _ => 500,
    });
    res.set_body(json!({ "error": e.to_string() }));
    res.set_error(tide::http::Error::from(e));
    res
}

async fn get_updates(req: tide::Request<State>) -> tide::Result {
    let id = req.param("id")?.to_string();
    let range = req.param("range")?;
    let (start, end, poll) = {
        if let Some((start, end)) = range.split_once("..") {
            if end.is_empty() {
                // start..
                let start = start.parse::<updates::Serial>()?;
                (start, start+1, true)
            } else {
                // start..end
                let start = start.parse::<updates::Serial>()?;
                let end = end.parse::<updates::Serial>()?;
                (start, end, false)
            }
        } else {
            // serial
            let serial = range.parse::<updates::Serial>()?;
            (serial, serial+1, false)
        }
    };
    let body = {
        if poll {
            if let Some(update) = req.state().updates.poll(&id, start).await? {
                update_to_bytes(&update)
            } else {
                vec![]
            }
        } else {
            let updates = req.state().updates.get(&id, start, end).await?;
            updates.iter().map(update_to_bytes).collect::<Vec<_>>().concat()
        }
    };
    Ok(tide::Response::builder(tide::StatusCode::Ok).body(body).build())
}

async fn push_update(mut req: tide::Request<State>) -> tide::Result {
    let id = req.param("id")?.to_string();
    let mut data = vec![];
    io::copy(&mut req, &mut data).await?;
    let serial = req.state().updates.push(&id, &data).await?;
    Ok(json!({ "ok": true, "serial": serial }).into())
}

fn update_to_bytes((serial,data): &updates::Update) -> Vec<u8> {
    [
        (data.len() as u32).to_be_bytes().to_vec(),
        (*serial as u32).to_be_bytes().to_vec(),
        data.to_vec(),
    ].concat()
}

async fn handle_socket(
    req: tide::Request<State>,
    stream: WebSocketConnection
) -> Result<(), tide::Error> {
    let socket_id = req.param("id")?.to_string();
    let channel = req.state().channels.open(&socket_id).await.unwrap();
    let id = channel.id;

    let mut tasks = vec![];
    {
        let mut stream = stream.clone();
        let channel = channel.clone();
        tasks.push(task::spawn(async move {
            while let Some(Ok(msg)) = stream.next().await {
                match msg {
                    Message::Binary(input) => {
                        channel.send(&input).await.unwrap();
                    },
                    Message::Text(_) | Message::Ping(_) | Message::Pong(_) => {},
                    Message::Close(_) => break,
                }
            }
            Ok(())
        }));
    }
    {
        let stream = stream.clone();
        let channel = channel.clone();
        tasks.push(task::spawn(async move {
            loop {
                let Ok(data) = channel.recv().await else { break };
                if stream.send(Message::Binary(data)).await.is_err() { break }
            }
            Ok(())
        }));
    }
    let (result, i, mut tasks) = future::select_all(tasks).await;

    for (j,t) in tasks.drain(..).enumerate() {
        if i != j { t.cancel().await; }
    }

    req.state().channels.close(&socket_id, id).await.unwrap();
    result
}
