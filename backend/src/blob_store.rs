use async_std::{fs, io, io::{ReadExt,WriteExt}};
use futures::io::{AsyncRead,AsyncBufRead};
use std::path::PathBuf;
use crate::config::Config;

#[derive(Clone)]
pub struct BlobStore {
    basedir: std::path::PathBuf,
    max_blob_size: u64,
}

impl BlobStore {
    pub fn new(config: &Config) -> Self {
        let mut basedir: PathBuf = config.data_dir.clone().into();
        basedir.push("data");
        Self {
            basedir,
            max_blob_size: config.max_blob_size,
        }
    }

    pub async fn put(&self, id: &str, reader: &mut (dyn AsyncRead+Unpin+Send+Sync)) -> Result<(),io::Error> {
        Self::check_id(id)?;
        let mut dst = self.basedir.clone();
        dst.push(&id[0..2]);
        dst.push(&id[2..4]);
        fs::DirBuilder::new().recursive(true).create(&dst).await?;
        dst.push(&id[4..]);

        let extra = 27; // magic bytes (3) + nonce bytes (24)
        let mut offset: u64 = 0;
        let mut writer = fs::File::create(&dst).await?;
        let mut data = vec![0;8192];
        loop {
            let n = reader.read(&mut data).await?;
            if n == 0 { break }
            if offset == 0 && (n < 3 || data[0..3] != vec![0x77, 0xeb, 0xdc]) {
                fs::remove_file(&dst).await?;
                return Err(io::Error::new(io::ErrorKind::InvalidInput, "not a webxdc blob"));
            }
            offset += n as u64;
            if offset > self.max_blob_size + extra {
                fs::remove_file(&dst).await?;
                return Err(io::Error::new(io::ErrorKind::InvalidInput, "blob too large"));
            }
            writer.write_all(&data[0..n]).await?;
        }
        writer.flush().await?;
        if offset < extra {
            fs::remove_file(&dst).await?;
            return Err(io::Error::new(io::ErrorKind::InvalidInput, "blob missing nonce"));
        }
        Ok(())
    }

    pub async fn get(&self, id: &str) -> Result<Box<dyn AsyncBufRead+Unpin+Send+Sync>,io::Error> {
        Self::check_id(id)?;
        let mut src = self.basedir.clone();
        src.push(&id[0..2]);
        src.push(&id[2..4]);
        src.push(&id[4..]);
        Ok(Box::new(io::BufReader::new(fs::File::open(&src).await?)))
    }

    pub async fn delete(&self, id: &str) -> Result<(),io::Error> {
        Self::check_id(id)?;
        let mut src = self.basedir.clone();
        src.push(&id[0..2]);
        src.push(&id[2..4]);
        src.push(&id[4..]);
        fs::remove_file(&src).await
    }

    fn check_id(id: &str) -> Result<(),io::Error> {
        if id.len() != 64 {
            return Err(io::Error::new(io::ErrorKind::InvalidInput, "id must be 64 hex digits"));
        }
        let valid = id.chars().all(|c| {
            c.is_ascii_hexdigit() && (c.is_ascii_lowercase() || c.is_ascii_digit())
        });
        if !valid {
            return Err(io::Error::new(
                io::ErrorKind::InvalidInput,
                "blob id must be a hex string",
            ));
        }
        Ok(())
    }
}
