use clap::Parser;

#[derive(Parser, Debug)]
pub struct Args {
    #[arg(short, long, default_value="127.0.0.1:8080")]
    listen: String,

    #[arg(long, default_value_t=5_242_880)]
    max_blob_size: u64,

    #[arg(short, long, default_value=".")]
    data_dir: String,
}

#[derive(Clone, Debug)]
pub struct Config {
    pub listen: String,
    pub max_blob_size: u64,
    pub data_dir: String,
}

impl From<Args> for Config {
    fn from(args: Args) -> Config {
        Config {
            listen: args.listen.clone(),
            max_blob_size: args.max_blob_size,
            data_dir: args.data_dir.clone(),
        }
    }
}
