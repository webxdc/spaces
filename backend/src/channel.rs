use std::collections::HashMap;
use async_std::{
    task,
    sync::{Arc,RwLock,Mutex},
    channel::{Sender,Receiver,bounded,unbounded},
};

type Error = Box<dyn std::error::Error + Send + Sync>;

pub struct Channel {
    sender: Sender<(u64,Vec<u8>)>,
    sessions: Arc<RwLock<HashMap<u64,Sender<Vec<u8>>>>>,
    next_id: u64,
    pub count: u64,
    worker: Arc<Mutex<Option<task::JoinHandle<()>>>>,
}

impl Default for Channel {
    fn default() -> Self {
        let (sender, receiver) = unbounded();
        let sessions: Arc<RwLock<HashMap<u64,Sender<Vec<u8>>>>> = Arc::new(RwLock::new(HashMap::default()));
        let worker = {
            let sessions = sessions.clone();
            task::spawn(async move {
                loop {
                    let (msg_id,msg): (u64,Vec<u8>) = receiver.recv().await.unwrap();
                    for (id,sender) in sessions.read().await.iter() {
                        if msg_id != *id {
                            sender.send(msg.clone()).await.unwrap();
                        }
                    }
                }
            })
        };
        Self {
            sender,
            sessions,
            next_id: 0,
            count: 0,
            worker: Arc::new(Mutex::new(Some(worker))),
        }
    }
}

impl Drop for Channel {
    fn drop(&mut self) {
        let worker = self.worker.clone();
        task::spawn(async move {
            worker.lock().await.take().unwrap().cancel().await;
        });
    }
}

impl Channel {
    pub async fn open_session(&mut self) -> ChannelSession {
        let id = self.next_id;
        self.next_id += 1;
        self.count += 1;
        let (sender, receiver) = bounded(16);
        self.sessions.write().await.insert(id, sender.clone());
        ChannelSession::new(
            id,
            self.sender.clone(),
            receiver,
        )
    }
    pub async fn close_session(&mut self, id: u64) {
        if self.sessions.read().await.get(&id).is_some() {
            self.sessions.write().await.remove(&id);
            self.count -= 1;
        }
    }
}

#[derive(Clone)]
pub struct ChannelSession {
    pub id: u64,
    up: Sender<(u64, Vec<u8>)>,
    receiver: Receiver<Vec<u8>>,
}

impl ChannelSession {
    pub fn new(
        id: u64,
        up: Sender<(u64,Vec<u8>)>,
        receiver: Receiver<Vec<u8>>,
    ) -> Self {
        Self { id, up, receiver }
    }
    pub async fn send(&self, data: &[u8]) -> Result<(),Error> {
        self.up.send((self.id, data.to_vec())).await.map_err(|e| e.into())
    }
    pub async fn recv(&self) -> Result<Vec<u8>,Error> {
        self.receiver.recv().await.map_err(|e| e.into())
    }
}

#[derive(Default,Clone)]
pub struct Channels {
    channels: Arc<RwLock<HashMap<String,Channel>>>,
}

impl Channels {
    pub async fn open(&self, id: &str) -> Result<ChannelSession,Error> {
        let mut channels = self.channels.write().await;
        let ch = channels.entry(id.to_string()).or_default();
        Ok(ch.open_session().await)
    }

    pub async fn close(&self, ch_id: &str, id: u64) -> Result<(),Error> {
        let mut channels = self.channels.write().await;
        if let Some(ch) = channels.get_mut(ch_id) {
            ch.close_session(id).await;
            if ch.count == 0 {
                channels.remove(ch_id);
            }
        }
        Ok(())
    }
}
