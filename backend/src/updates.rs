use async_std::{sync::{Arc,Mutex}, prelude::FutureExt};
use async_rusqlite::{Connection,rusqlite};
use std::collections::HashMap;
use async_broadcast::{Sender, Receiver};
use std::path::PathBuf;
use crate::config::Config;
use std::time::Duration;

#[derive(Clone)]
pub struct Updates {
    pub db: Connection,
    pubsub: PubSub,
}
type PubSubPair = (
    Sender<(Serial, Vec<u8>)>,
    Receiver<(Serial, Vec<u8>)>,
);
type E = async_rusqlite::Error;
pub type Serial = i64;
pub type Update = (Serial, Vec<u8>);
type Id = str;

#[derive(Default,Clone)]
struct PubSub {
    subscriptions: Arc<Mutex<HashMap<String,PubSubPair>>>,
    counts: Arc<Mutex<HashMap<String,usize>>>,
}

impl PubSub {
    pub async fn open(&self, socket_id: &Id) -> PubSubPair {
        let mut subs = self.subscriptions.lock().await;
        let pubsub = subs.get(socket_id).cloned().unwrap_or_else(|| {
            let (send, recv) = async_broadcast::broadcast(10);
            subs.insert(socket_id.to_string(), (send.clone(), recv.clone()));
            (send, recv)
        });
        *self.counts.lock().await.entry(socket_id.to_string()).or_insert(0) += 1;
        pubsub
    }

    pub async fn close(&self, socket_id: &Id) {
        let mut counts = self.counts.lock().await;
        let count = counts.entry(socket_id.to_string()).or_insert(1);
        *count -= 1;
        if *count == 0 {
            let mut subs = self.subscriptions.lock().await;
            subs.remove(socket_id);
        }
    }
}

impl Updates {
    pub async fn open(config: &Config) -> Result<Self,E> {
        let mut dbfile: PathBuf = config.data_dir.clone().into();
        dbfile.push("db");
        let db = Connection::open(dbfile).await.unwrap();
        db.call(|conn| {
            conn.execute("create table if not exists updates (
                id char[32],
                serial integer,
                data BLOB
            )", ())
        }).await?;
        Ok(Self {
            db,
            pubsub: PubSub::default(),
        })
    }

    pub async fn get(&self, id: &Id, start: Serial, end: Serial) -> Result<Vec<Update>,E> {
        let db = self.db.clone();
        let id = id.to_string();
        db.call(move |conn| {
            let mut stmt = if end >= 0 {
                conn.prepare("
                    select serial, data from updates where id = ?1 and ?2 <= serial and serial < ?3
                ")?
            } else {
                conn.prepare("
                    select serial, data from updates where id = ?1 and ?2 <= serial
                ")?
            };
            let mut rows = if end >= 0 {
                stmt.query(rusqlite::params![id, start, end])?
            } else {
                stmt.query(rusqlite::params![id, start])?
            };
            let mut updates = vec![];
            while let Some(row) = rows.next()? {
                let Ok(serial): Result<Serial,_> = row.get("serial") else { continue };
                let Ok(data): Result<Vec<u8>,_> = row.get("data") else { continue };
                updates.push((serial, data));
            }
            Ok::<_,E>(updates)
        }).await
    }

    pub async fn poll(&self, id: &Id, serial: Serial) -> Result<Option<Update>,E> {
        let db = self.db.clone();
        let id = id.to_string();
        let r = {
            let id = id.clone();
            db.call(move |conn| {
                let mut stmt = conn.prepare("
                    select serial, data from updates where id = ?1 and serial = ?2
                ")?;
                let mut rows = stmt.query(rusqlite::params![id, serial])?;
                if let Some(row) = rows.next()? {
                    let Ok(serial): Result<Serial,_> = row.get("serial") else { return Ok(None) };
                    let Ok(data): Result<Vec<u8>,_> = row.get("data") else { return Ok(None) };
                    Ok::<_, async_rusqlite::Error>(Some((serial, data)))
                } else {
                    Ok::<_, async_rusqlite::Error>(None)
                }
            }).await?
        };
        if r.is_some() {
            Ok(r)
        } else {
            let (_send, mut recv) = self.pubsub.open(&id).await;
            // only wait for 1 update for now:
            let Ok(Ok((update_serial, update_data))) = recv.recv()
                .timeout(Duration::from_secs(100)).await
                else { return Ok(None) };
            self.pubsub.close(&id).await;
            if update_serial == serial || serial == 0 {
                Ok(Some((update_serial, update_data)))
            } else {
                Ok(None)
            }
        }
    }

    pub async fn push(&self, id: &Id, data: &[u8]) -> Result<Serial,E> {
        let serial = {
            let db = self.db.clone();
            let id = id.to_string();
            let data = data.to_vec();
            db.call(move |conn| {
                let tx = conn.transaction()?;
                let mut stmt = tx.prepare("select max(serial) from updates where id = ?1")?;
                let mut rows = stmt.query(rusqlite::params![id.clone()])?;
                let serial: Serial = rows.next()?.and_then(|r| r.get(0).ok()).unwrap_or(0i64) + 1;
                drop(rows);
                tx.execute(
                    "insert into updates (id, serial, data) values (?1, ?2, ?3)",
                    rusqlite::params![id, serial, data]
                )?;
                drop(stmt);
                tx.commit()?;
                Ok::<Serial,E>(serial)
            }).await?
        };
        let (send, _recv) = self.pubsub.open(id).await;
        send.broadcast((serial, data.to_vec())).await.unwrap();
        self.pubsub.close(id).await;
        Ok::<Serial,E>(serial)
    }
}
