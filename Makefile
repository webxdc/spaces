all: build-frontend run-backend

run-backend:
	cargo run --manifest-path backend/Cargo.toml -- $(args)

build-backend:
	cargo build --manifest-path backend/Cargo.toml

build-frontend:
	frontend/node_modules/.bin/browserify frontend/main.js -o public/_webxdc/bundle.js

install-frontend:
	(cd frontend; npm install)

dev-frontend:
	frontend/node_modules/.bin/watchify frontend/main.js -o public/_webxdc/bundle.js -dv
