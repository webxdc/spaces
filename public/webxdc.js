window.webxdc = (() => {
  let updateListener = (_) => {}
  let serial = -1
  let importCb = null

  let socket = null
  let socketQueue = []
  async function getSocket() {
    return new Promise((respond,reject) => {
      if (socket) respond(socket)
      else socketQueue.push(respond)
    })
  }

  class RealtimeChannel {
    // TODO: move websocket into main thread for encryption
    constructor() {
      this._isOpen = false
      this._onOpen = () => {
        this._isOpen = true
        this._queue.forEach(data => {
          this._socket.send(data)
        })
        this._queue = []
      }
      this._onClose = () => { this._isOpen = false }
      this._onError = err => {
        console.error(err)
      }
      this._onMessage = async ev => {
        let data = new Uint8Array(await blobToArrayBuffer(ev.data))
        if (this._listener) this._listener(data)
      }
      this._queue = []
      this._listener = null
      this._socket = null
    }
    _setURL(url) {
      this._socket = new WebSocket(url)
      this._socket.addEventListener('open', this._onOpen)
      this._socket.addEventListener('close', this._onClose)
      this._socket.addEventListener('error', this._onError)
      this._socket.addEventListener('message', this._onMessage)
    }
    setListener(f) {
      this._listener = f
    }
    send(data) {
      if (this._isOpen) this._socket.send(data)
      else this._queue.push(data)
    }
    leave() {
      this._socket.removeEventListener('open', this._onOpen)
      this._socket.removeEventListener('close', this._onClose)
      this._socket.removeEventListener('error', this._onError)
      this._socket.removeEventListener('message', this._onMessage)
      this._socket.close()
    }
  }

  window.addEventListener('message', ev => {
    if (ev.data && ev.data.type === 'info') {
      serial = ev.data.serial
      socket = ev.data.socket
      socketQueue.forEach(f => f(socket))
      socketQueue = []
    } else if (ev.data && ev.data.type === 'update') {
      let update = Object.assign({ serial: ev.data.serial }, ev.data.data)
      serial = Math.max(serial, ev.data.serial)
      updateListener(update)
    } else if (ev.data && ev.data.type === 'import-files-reply' && ev.data.error) {
      importCb.reject(ev.data.error)
      importCb = null
    } else if (ev.data && ev.data.type === 'import-files-reply' && ev.data.files) {
      importCb.resolve(ev.data.files)
      importCb = null
    }
  })

  let selfName = u8ToHex(crypto.getRandomValues(new Uint8Array(4)))
  return {
    selfName,
    selfAddr: `${selfName}@${location.host}`,
    setUpdateListener: (cb, serial = 0) => {
      window.parent.postMessage({ type:  'setUpdateListener', serial })
      updateListener = cb
      return Promise.resolve()
    },
    getAllUpdates: () => {
      throw new Error('deprecated method not currently supported')
    },
    sendUpdate: (data, description) => {
      window.parent.postMessage({ type: 'sendUpdate', data })
    },
    sendToChat: async (data) => {
      window.parent.postMessage({ type: 'sendToChat', data })
      return Promise.resolve() // TODO: route async result
    },
    importFiles: (filter = {}) => {
      window.parent.postMessage({ type:  'importFiles', filter })
      return new Promise((resolve,reject) => {
        importCb = { resolve, reject }
      })
    },
    joinRealtimeChannel: () => {
      let ch = new RealtimeChannel(socket)
      getSocket().then(url => ch._setURL(url))
      return ch
    },
  }

  async function blobToArrayBuffer(blob) {
    return new Promise((resolve,reject) => {
      let fr = new FileReader
      fr.readAsArrayBuffer(blob)
      fr.addEventListener('load', ev => {
        resolve(ev.target.result)
      })
      fr.addEventListener('error', reject)
    })
  }

  function u8ToHex(bytes) {
    let out = ''
    for (let i = 0; i < bytes.length; i++) {
      out += bytes[i].toString(16).padStart(2, '0')
    }
    return out
  }
})()
