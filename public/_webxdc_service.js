self.addEventListener('error', err => { console.log('ERROR', err) })
let installed = null

const MIME = new Map(Object.entries({
  html: 'text/html',
  htm: 'text/html',
  js: 'application/javascript',
  css: 'text/css',
  txt: 'text/plain',
  json: 'application/json',
  wasm: 'application/wasm',
  png: 'image/png',
  jpg: 'image/jpeg',
  jpeg: 'image/jpeg',
  gif: 'image/gif',
  svg: 'image/svg+xml',
  webp: 'image/webp',
  mp3: 'audio/mpeg',
  m4a: 'audio/mp4',
  mp4: 'audio/mp4',
  wav: 'audio/wav',
  ogg: 'audio/ogg',
  mpeg: 'video/mpeg',
  webm: 'video/webm',
  ogv: 'video/ogg',
}))

self.addEventListener('message', ev => {
  if (ev.data && ev.data.type === 'install' && typeof ev.data.id === 'string') {
    console.log('INSTALL', ev.data)
    installed = {
      id: ev.data.id,
      files: ev.data.files,
      time: Date.now(),
    }
    ev.ports[0].postMessage({ ok: true })
  } else if (ev.data && ev.data.type === 'uninstall' && typeof ev.data.id === 'string') {
    console.log('UNINSTALL', ev.data.id)
    installed = null
  }
})

self.addEventListener('install', ev => {
  console.log('SERVICE:INSTALL', ev)
})

self.addEventListener('activate', ev => {
  console.log('SERVICE:ACTIVATE', ev)
})

self.addEventListener('fetch', ev => {
  console.log('SERVICE:FETCH', ev)
  if (installed === null) return
  try {
    let url = new URL(ev.request.url)
    if (ev.request.method !== 'GET') return
    if (url.pathname.startsWith('/blob/')) return
    if (url.pathname.startsWith('/_webxdc/')) return
    if (url.pathname.startsWith('/_webxdc_')) return
    if (url.pathname === '/') return
    if (url.pathname === '/webxdc.js') return

    let pathname = url.pathname.slice(1)
    if (pathname === 'app.html') pathname = 'index.html'

    let body = installed.files.get(pathname)
    if (body === undefined) return notFound()

    let mext = /\.([\w-]+)$/.exec(pathname)
    let ext = mext ? mext[1].toLowerCase() : null
    let headers = { 'Content-Type': MIME.get(ext) ?? 'application/octet-stream' }
    ev.respondWith(new Response(body, { headers }))
  } catch (err) {
    return ev.respondWith(new Response(String(err), { status: 500 }))
  }

  function notFound(msg) {
    return ev.respondWith(new Response(msg ?? 'file not found', { status: 404 }))
  }
})
